# README #

Anomaly detection in Python

### What is this repository for? ###

* Anomaly detection in Python (jupyter notebook)
* 1.0

### Covering ... ###

- Read.ipynb: 
	* Read and process raw datasets
- Anomaly.ipynb:
	* Calculating (univariate or multivariate) time series Euclidean distance 
	* Community Detection (Louvain Method) + K-Nearest Neighbors 
	* Optimization approaches for Louvain Hyperparameters
	* Anomaly Detection algorithm
- Read_hourly.ipynb:
	* Read data hourly - Community Detection - Anomaly detection

### Dataset ###

* Input a dataset 

### Who do I talk to? ###

Konstantinos Kousias - kostas@simula.no